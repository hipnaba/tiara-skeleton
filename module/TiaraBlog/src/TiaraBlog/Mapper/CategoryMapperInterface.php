<?php
namespace TiaraBlog\Mapper;

use TiaraBase\Mapper\MapperInterface;

interface CategoryMapperInterface extends
    MapperInterface
{
    /**
     * @param string $name
     * @return null|\TiaraBlog\Entity\CategoryInterface
     */
    public function findByName($name);
}