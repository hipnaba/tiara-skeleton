<?php
namespace TiaraBlog\Mapper;

use TiaraBase\Mapper\AbstractMapper;

class CategoryMapper extends AbstractMapper implements
    CategoryMapperInterface
{
    protected $entityPrototypeName = 'Category';
    protected $table = 'blog_category';

    /**
     * @param string $name
     * @return null|\TiaraBlog\Entity\CategoryInterface
     */
    public function findByName($name)
    {
        return $this->findOneBy('name', $name);
    }
}