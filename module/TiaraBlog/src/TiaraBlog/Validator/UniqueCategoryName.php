<?php
namespace TiaraBlog\Validator;

use TiaraBlog\Mapper\CategoryMapperInterface;
use Zend\Validator\AbstractValidator;
use Zend\Validator\Exception;

class UniqueCategoryName extends AbstractValidator
{
    /** @var CategoryMapperInterface */
    protected $categories;

    const CATEGORY_EXISTS = 'categoryExists';

    protected $messageTemplates = array(
        self::CATEGORY_EXISTS => 'A category with this name already exists'
    );

    /**
     * Constructor
     *
     * @param CategoryMapperInterface $categoryMapper
     * @param null $options
     */
    function __construct(CategoryMapperInterface $categoryMapper, $options = null)
    {
        parent::__construct($options);

        $this->categories = $categoryMapper;
    }

    /**
     * Returns true if the username doesn't exists in the database or the username is of the
     * user we're trying to save
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param  string $value
     * @param array $context
     * @return bool
     */
    public function isValid($value, $context = array())
    {
        $user = $this->categories->findByName($value);

        if (null !== $user) {
            if ((isset($context['id']) && $context['id'] != $user->getId()) || !isset($context['id'])) {
                $this->error(static::CATEGORY_EXISTS);
                return false;
            }
        }
        return true;
    }
}