<?php
namespace TiaraBlog\Validator;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class UniqueCategoryNameFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var \Zend\Validator\ValidatorPluginManager $validators */
        $validators = $serviceLocator;
        $services = $validators->getServiceLocator();
        /** @var \TiaraBase\Mapper\MapperManager $mappers */
        $mappers = $services->get('MapperManager');
        /** @var \TiaraBlog\Mapper\CategoryMapperInterface $categoryMapper */
        $categoryMapper = $mappers->get('Category');

        return new UniqueCategoryName($categoryMapper);
    }
}