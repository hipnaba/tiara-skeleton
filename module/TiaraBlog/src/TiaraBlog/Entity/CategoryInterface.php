<?php
namespace TiaraBlog\Entity;

use TiaraBase\Entity\EntityInterface;

interface CategoryInterface extends
    EntityInterface
{
    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name);
}