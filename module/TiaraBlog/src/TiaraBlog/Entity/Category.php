<?php
namespace TiaraBlog\Entity;

use TiaraBase\Entity\AbstractEntity;

class Category extends AbstractEntity implements
    CategoryInterface
{
    /** @var string */
    protected $name;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = (string) $name;
        return $this;
    }
}