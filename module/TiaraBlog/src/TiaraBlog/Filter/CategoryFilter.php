<?php
namespace TiaraBlog\Filter;

use Zend\InputFilter\InputFilter;
use Zend\Validator\NotEmpty;

class CategoryFilter extends InputFilter
{
    public function init()
    {
        $this->add(array(
            'name' => 'id',
            'required' => true,
            'filters' => array(
                array('name' => 'Int')
            )
        ));

        $this->add(array(
            'name' => 'name',
            'required' => true,
            'filters' => array(
                array('name' => 'StringTrim'),
                array('name' => 'StripTags')
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            NotEmpty::IS_EMPTY => 'Category name is required and can\'t be empty'
                        )
                    )
                ),
                array('name' => 'UniqueCategoryName')
            )
        ));
    }
}