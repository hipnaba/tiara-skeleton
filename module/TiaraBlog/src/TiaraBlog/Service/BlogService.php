<?php
namespace TiaraBlog\Service;

use TiaraBase\Mapper\MapperManager;
use TiaraBlog\Entity\CategoryInterface;
use Zend\Form\FormElementManager;

class BlogService
{
    /** @var \TiaraBlog\Mapper\CategoryMapperInterface */
    protected $categories;
    /** @var \Zend\Form\FormElementManager */
    protected $forms;
    /** @var \Zend\Form\Form */
    protected $addCategoryForm;

    function __construct(MapperManager $mappers, FormElementManager $forms)
    {
        $this->categories = $mappers->get('Category');
        $this->forms = $forms;
    }

    /**
     * @param array $data
     * @return null|CategoryInterface
     */
    public function addCategory(array $data)
    {
        $form = $this->getAddCategoryForm();

        /** @var CategoryInterface $category */
        $category = $this->categories->getEntityPrototype();
        $form->bind($category);

        $form->setData($data);
        if (!$form->isValid()) {
            return null;
        }

        $this->categories->save($category);
        return $category;
    }

    /**
     * @return \Zend\Form\Form
     */
    public function getAddCategoryForm()
    {
        if (null === $this->addCategoryForm) {
            $this->addCategoryForm = $this->forms->get('Blog\Form\AddCategory');
            $this->addCategoryForm->setHydrator($this->categories->getHydrator());
        }
        return $this->addCategoryForm;
    }
}