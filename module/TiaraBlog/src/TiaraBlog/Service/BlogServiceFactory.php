<?php
namespace TiaraBlog\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class BlogServiceFactory implements
    FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var \TiaraBase\Mapper\MapperManager $mappers */
        $mappers = $serviceLocator->get('MapperManager');
        /** @var \Zend\Form\FormElementManager $forms */
        $forms = $serviceLocator->get('FormElementManager');

        return new BlogService($mappers, $forms);
    }
}