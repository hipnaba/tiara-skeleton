<?php
namespace TiaraBlog\Form;

use TiaraBase\Form\AbstractForm;

class BaseCategoryForm extends AbstractForm
{
    protected $filter = 'Blog\Filter\Category';

    public function init()
    {
        $this->add(array(
            'type' => 'hidden',
            'name' => 'id'
        ));

        $this->add(array(
            'type' => 'text',
            'name' => 'name',
            'options' => array(
                'label' => 'Name'
            ),
            'attributes' => array(
                'placeholder' => 'Enter category name',
            )
        ));

        $this->add(array(
            'type' => 'submit',
            'name' => 'save',
            'options' => array(
                'label' => 'Save'
            )
        ), array('priority' => -999));

        $this->add(array(
            'type' => 'submit',
            'name' => 'cancel',
            'options' => array(
                'label' => 'Cancel'
            )
        ), array('priority' => -1000));
    }
}