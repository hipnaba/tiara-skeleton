<?php
namespace TiaraBlog\Form;

class AddCategoryForm extends BaseCategoryForm
{
    public function init()
    {
        parent::init();

        $this->remove('id');
        $this->get('save')->setLabel('Add category');
    }
}