<?php
namespace TiaraBlog\Controller;

use Zend\Mvc\Controller\AbstractActionController;

class PostController extends AbstractActionController
{
    public function indexAction()
    { }

    public function addAction()
    { }

    public function editAction()
    { }

    public function deleteAction()
    { }
}