<?php
namespace TiaraBlog\Controller;

use TiaraBlog\Service\BlogService;
use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractActionController;

/**
 * Class CategoryController
 * @package TiaraBlog\Controller
 * @method BlogService blog()
 */
class CategoryController extends AbstractActionController
{
    public function addAction()
    {
        $form = $this->blog()->getAddCategoryForm();

        $redirect = $this->url()->fromRoute('admin/blog/category', array('action' => 'add'));
        $prg = $this->prg($redirect, true);

        if ($prg instanceof Response) {
            return $prg;
        }

        if (false !== $prg) {
            if (isset($prg['cancel'])) {
                // TODO: go back instead of index
                return $this->redirect()->toUrl($this->url()->fromRoute('admin/blog/index'));
            }

            $category = $this->blog()->addCategory($prg);
            if (null !== $category) {
                $this->flashMessenger('tiara-admin')->addSuccessMessage("Category added");
                return $this->redirect()->toUrl($this->url()->fromRoute('admin/blog/index', array(
                    'category' => $category->getId()
                )));
            }
        }

        return array('form' => $form);
    }

    public function editAction()
    { }

    public function deleteAction()
    { }
}