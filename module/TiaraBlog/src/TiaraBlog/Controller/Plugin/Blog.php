<?php
namespace TiaraBlog\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class Blog extends AbstractPlugin implements
    ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /** @var \TiaraBlog\Service\BlogService */
    protected $service;

    /**
     * @return \TiaraBlog\Service\BlogService
     */
    public function __invoke()
    {
        if (null === $this->service) {
            /** @var \Zend\Mvc\Controller\PluginManager $plugins */
            $plugins = $this->getServiceLocator();
            /** @var \Zend\ServiceManager\ServiceLocatorInterface $services */
            $services = $plugins->getServiceLocator();
            $this->service = $services->get('Blog');
        }
        return $this->service;
    }
}