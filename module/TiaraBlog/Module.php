<?php
namespace TiaraBlog;

use TiaraBase\Entity\EntityProviderInterface;
use TiaraBase\Mapper\MapperProviderInterface;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ControllerPluginProviderInterface;
use Zend\ModuleManager\Feature\ControllerProviderInterface;
use Zend\ModuleManager\Feature\FormElementProviderInterface;
use Zend\ModuleManager\Feature\InputFilterProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\ValidatorProviderInterface;

class Module implements
    AutoloaderProviderInterface,
    ConfigProviderInterface,
    ControllerProviderInterface,
    ControllerPluginProviderInterface,
    EntityProviderInterface,
    FormElementProviderInterface,
    InputFilterProviderInterface,
    MapperProviderInterface,
    ServiceProviderInterface,
    ValidatorProviderInterface
{
    /**
     * Return an array for passing to Zend\Loader\AutoloaderFactory.
     *
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
                )
            )
        );
    }

    /**
     * Returns configuration to merge with application configuration
     *
     * @return array|\Traversable
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to seed
     * such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getControllerConfig()
    {
        return array(
            'invokables' => array(
                'Blog\Controller\Admin' => 'TiaraBlog\Controller\AdminController',
                'Blog\Controller\Category' => 'TiaraBlog\Controller\CategoryController',
                'Blog\Controller\Post' => 'TiaraBlog\Controller\PostController',
            )
        );
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getControllerPluginConfig()
    {
        return array(
            'aliases' => array(
                'Blog' => 'Blog\Plugin\Blog'
            ),
            'invokables' => array(
                'Blog\Plugin\Blog' => 'TiaraBlog\Controller\Plugin\Blog'
            )
        );
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getEntityConfig()
    {
        return array(
            'aliases' => array(
                'Blog\Entity\Category' => 'TiaraBlog\Entity\CategoryInterface',
                /** short */
                'Category' => 'Blog\Entity\Category',
            ),
            'invokables' => array(
                'TiaraBlog\Entity\CategoryInterface' => 'TiaraBlog\Entity\Category'
            )
        );
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getFormElementConfig()
    {
        return array(
            'invokables' => array(
                'Blog\Form\AddCategory' => 'TiaraBlog\Form\AddCategoryForm',
            )
        );
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getInputFilterConfig()
    {
        return array(
            'invokables' => array(
                'Blog\Filter\Category' => 'TiaraBlog\Filter\CategoryFilter'
            )
        );
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getMapperConfig()
    {
        return array(
            'aliases' => array(
                'Blog\Mapper\Category' => 'TiaraBlog\Entity\CategoryMapperInterface',
                /** short */
                'Category' => 'Blog\Mapper\Category'
            ),
            'invokables' => array(
                'TiaraBlog\Entity\CategoryMapperInterface' => 'TiaraBlog\Mapper\CategoryMapper'
            )
        );
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getServiceConfig()
    {
        return array(
            'aliases' => array(
                'Blog' => 'Blog\Service\Blog',
            ),
            'factories' => array(
                'Blog\Service\Blog' => 'TiaraBlog\Service\BlogServiceFactory'
            )
        );
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getValidatorConfig()
    {
        return array(
            'aliases' => array(
                'UniqueCategoryName' => 'Blog\Validator\UniqueCategoryName',
            ),
            'factories' => array(
                'Blog\Validator\UniqueCategoryName' => 'TiaraBlog\Validator\UniqueCategoryNameFactory'
            )
        );
    }
}