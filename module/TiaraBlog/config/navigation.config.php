<?php
return array(
    'admin' => array(
        'home' => array(
            'pages' => array(
                array(
                    'label' => 'Blog',
                    'route' => 'admin/blog',
                    'pages' => array(
                        array(
                            'label' => 'Add category',
                            'route' => 'admin/blog/category',
                            'params' => array(
                                'action' => 'add'
                            )
                        ),
                        array(
                            'label' => 'Edit category',
                            'route' => 'admin/blog/category',
                            'visible' => false,
                            'params' => array(
                                'action' => 'edit'
                            )
                        ),
                        array(
                            'label' => 'Delete category',
                            'route' => 'admin/blog/category',
                            'visible' => false,
                            'params' => array(
                                'action' => 'delete'
                            )
                        ),

                        array(
                            'label' => 'Add post',
                            'route' => 'admin/blog/post',
                            'params' => array(
                                'action' => 'add'
                            )
                        ),
                        array(
                            'label' => 'Edit post',
                            'route' => 'admin/blog/post',
                            'visible' => false,
                            'params' => array(
                                'action' => 'edit'
                            )
                        ),
                        array(
                            'label' => 'Delete post',
                            'route' => 'admin/blog/post',
                            'visible' => false,
                            'params' => array(
                                'action' => 'delete'
                            )
                        ),
                    )
                )
            )
        )
    )
);