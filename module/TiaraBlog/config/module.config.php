<?php
return array(
    'navigation' => include __DIR__ . '/navigation.config.php',

    'router' => array(
        'routes' => include __DIR__ . '/routes.config.php'
    ),

    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view'
        )
    )
);