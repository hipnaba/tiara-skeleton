<?php
return array(
    'admin' => array(
        'child_routes' => array(
            'blog' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/blog',
                    'defaults' => array(
                        'controller' => 'Blog\Controller\Admin',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'index' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '[/:category]',
                            'defaults' => array(
                                'category' => null,
                            ),
                            'constraints' => array(
                                'category' => '[0-9]+'
                            )
                        )
                    ),
                    'category' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/categories[/:action[/:id]]',
                            'defaults' => array(
                                'controller' => 'Blog\Controller\Category'
                            ),
                            'constraints' => array(
                                'action' => '[a-z-]+',
                                'id' => '[0-9]+'
                            )
                        )
                    ),
                    'post' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/posts[/:action[/:id]]',
                            'defaults' => array(
                                'controller' => 'Blog\Controller\Post'
                            ),
                            'constraints' => array(
                                'action' => '[a-z-]+',
                                'id' => '[0-9]+'
                            )
                        ),
                    )
                )
            )
        )
    )
);