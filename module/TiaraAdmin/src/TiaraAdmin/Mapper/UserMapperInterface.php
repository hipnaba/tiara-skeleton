<?php
namespace TiaraAdmin\Mapper;

use TiaraBase\Mapper\MapperInterface;
use TiaraAdmin\Entity\UserInterface;

/**
 * Interface UserMapperInterface
 *
 * @package TiaraAdmin\Mapper
 * @method null|UserInterface findById($id)
 */
interface UserMapperInterface extends MapperInterface
{
    /**
     * Returns the user with the provided username or null if it doesn't exist
     *
     * @param string $username
     * @return null|UserInterface
     */
    public function findByUsername($username);
}