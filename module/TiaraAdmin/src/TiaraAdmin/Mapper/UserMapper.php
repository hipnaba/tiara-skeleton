<?php
namespace TiaraAdmin\Mapper;

use TiaraBase\Mapper\AbstractMapper;
use TiaraAdmin\Entity\UserInterface;

/**
 * Class UserMapper
 *
 * @package TiaraAdmin\Mapper
 */
class UserMapper extends AbstractMapper implements
    UserMapperInterface
{
    /** @var string */
    protected $entityPrototypeName = 'User';
    /** @var string */
    protected $table = 'admin_user';

    /**
     * Returns the user with the provided username or null if it doesn't exist
     *
     * @param string $username
     * @return null|UserInterface
     */
    public function findByUsername($username)
    {
        return $this->findOneBy('username', $username);
    }
}