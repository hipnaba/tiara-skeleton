<?php
namespace TiaraAdmin\Service;

use TiaraAdmin\Exception\UnAuthorizedException;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\ListenerAggregateTrait;
use Zend\Mvc\Application;
use Zend\Mvc\MvcEvent;
use Zend\Permissions\Acl\Acl;
use Zend\Permissions\Acl\Exception\InvalidArgumentException;
use Zend\Permissions\Acl\Resource\GenericResource;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\Stdlib\Response;
use Zend\Http\Response as HttpResponse;
use Zend\View\Model\ViewModel;

class AuthorizationService implements
    ListenerAggregateInterface,
    ServiceLocatorAwareInterface
{
    use ListenerAggregateTrait;
    use ServiceLocatorAwareTrait;

    const ERROR_UNAUTHENTICATED = 'error-unauthenticated';
    const ERROR_UNAUTHORIZED = 'error-unauthorized';

    /** @var Acl */
    protected $acl;
    /** @var AuthenticationService */
    protected $authenticationService;

    public function isAllowed($resource, $privilege = null)
    {
        try {
            if (!$this->getAcl()->hasResource($resource)) {
                $resource = implode('/', array_slice(explode('/', $resource), 0, 2));
            }
            return $this->getAcl()->isAllowed($this->getIdentity(), $resource, $privilege);
        } catch (InvalidArgumentException $e) {
            return false;
        }
    }

    /**
     * @return Acl
     */
    private function getAcl()
    {
        if (null === $this->acl) {
            $this->acl = new Acl();

            $this->acl->addRole(new Role('guest'))
                      ->addRole(new Role('admin'), array('guest'));

            $this->acl->addResource(new GenericResource('route/site'));
            $this->acl->addResource(new GenericResource('route/login'));
            $this->acl->addResource(new GenericResource('route/logout'));
            $this->acl->addResource(new GenericResource('route/admin'));


            $this->acl->allow('guest', 'route/site');
            $this->acl->allow('guest', 'route/login');
            $this->acl->allow('guest', 'route/logout');

            $this->acl->allow('admin', 'route/admin');
        }
        return $this->acl;
    }

    public function hasIdentity()
    {
        return $this->getAuthenticationService()->hasIdentity();
    }

    public function getIdentity()
    {
        return $this->hasIdentity() ? 'admin' : 'guest';
    }

    /**
     * @return AuthenticationService
     */
    public function getAuthenticationService()
    {
        if (null === $this->authenticationService) {
            $this->authenticationService = $this->getServiceLocator()->get('Authentication');
        }
        return $this->authenticationService;
    }

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the EventManager
     * implementation will pass this to the aggregate.
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach(MvcEvent::EVENT_ROUTE, array($this, 'onRoute'));
        $this->listeners[] = $events->attach(MvcEvent::EVENT_DISPATCH_ERROR, array($this, 'onDispatchError'), -5000);
    }

    public function onRoute(MvcEvent $event)
    {
        /** @var \Zend\Mvc\Application $application */
        $application = $event->getTarget();

        $match      = $event->getRouteMatch();
        $routeName  = $match->getMatchedRouteName();

        //if ($this->isAllowed('route/' . (preg_match('/(login|logout)$/', $routeName) ? $routeName : 'admin'))) {
        if ($this->isAllowed('route/' . $routeName)) {
            return;
        }

        $event->setError($this->hasIdentity() ? AuthorizationService::ERROR_UNAUTHORIZED : AuthorizationService::ERROR_UNAUTHENTICATED);
        $event->setParam('route', $routeName);
        $event->setParam('identity', $this->getIdentity());
        $event->setParam('exception', new UnAuthorizedException('You are not authorized to access ' . $routeName));

        $application->getEventManager()->trigger(MvcEvent::EVENT_DISPATCH_ERROR, $event);
    }

    public function onDispatchError(MvcEvent $event)
    {
        // Do nothing if the result is a response object
        /** @var \Zend\Mvc\Application $application */
        $application = $event->getTarget();
        $result   = $event->getResult();
        $response = $event->getResponse();
        $router = $event->getRouter();

        if ($result instanceof Response || ($response && ! $response instanceof HttpResponse)) {
            return;
        }

        // Common view variables
        $viewVariables = array(
            'error'      => $event->getParam('error'),
            'identity'   => $event->getParam('identity'),
        );

        switch ($event->getError()) {
            case static::ERROR_UNAUTHENTICATED:
                $url = $router->assemble(array(), array('name' => 'login'));
                $response = $response ?: new HttpResponse();

                $services = $application->getServiceManager();
                /** @var \Zend\Mvc\Controller\PluginManager $plugins */
                $plugins = $services->get('ControllerPluginManager');
                /** @var \Zend\Mvc\Controller\Plugin\FlashMessenger $flash */
                $flash = $plugins->get('FlashMessenger');

                /** @var \Zend\Mvc\Router\Http\RouteMatch $route */
                $route = $event->getParam('route-match');
                $redirect = $router->assemble($route->getParams(), array('name' => $route->getMatchedRouteName()));
                $flash->setNamespace('redirect')->addMessage($redirect);

                $response->getHeaders()->addHeaderLine('Location', $url);
                $response->setStatusCode(302);

                $event->setResponse($response);
                return;
            case static::ERROR_UNAUTHORIZED:
                $viewVariables['route'] = $event->getParam('route');
                break;
            case Application::ERROR_EXCEPTION:
                if (!($event->getParam('exception') instanceof UnAuthorizedException)) {
                    return;
                }

                $viewVariables['reason'] = $event->getParam('exception')->getMessage();
                $viewVariables['error']  = 'error-unauthorized';
                break;
            default:
                /*
                 * do nothing if there is no error in the event or the error
                 * does not match one of our predefined errors (we don't want
                 * our 403 template to handle other types of errors)
                 */

                return;
        }

        $model    = new ViewModel($viewVariables);
        $response = $response ?: new HttpResponse();

        $model->setTemplate('error/403');
        $event->getViewModel()->addChild($model);
        $response->setStatusCode(403);
        $event->setResponse($response);
    }
}