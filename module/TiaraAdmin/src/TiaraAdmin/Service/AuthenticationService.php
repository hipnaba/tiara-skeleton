<?php
namespace TiaraAdmin\Service;

use TiaraAdmin\Authentication\Adapter\TiaraUser;
use Zend\Authentication\AuthenticationService as ZendAuthenticationService;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class AuthenticationService extends ZendAuthenticationService implements
    ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /** @var \Zend\Form\Form */
    protected $loginForm;
    /** @var \TiaraAdmin\Mapper\UserMapperInterface $userMapper */
    protected $userMapper;

    public function login($credentials)
    {
        $userMapper = $this->getUserMapper();

        $adapter = new TiaraUser($userMapper);
        $adapter->setUsername($credentials['username'])
                ->setPassword($credentials['password']);

        $result = $this->authenticate($adapter);
        return $result->isValid();
    }

    /**
     * @return null|\TiaraAdmin\Entity\UserInterface
     */
    public function getUser()
    {
        if ($this->hasIdentity()) {
            return $this->getUserMapper()->findById($this->getIdentity());
        }
        return null;
    }

    /**
     * @return \TiaraAdmin\Mapper\UserMapperInterface
     */
    public function getUserMapper()
    {
        if (null === $this->userMapper) {
            /** @var \TiaraBase\Mapper\MapperManager $mappers */
            $mappers = $this->serviceLocator->get('MapperManager');

            /** @var \TiaraAdmin\Mapper\UserMapperInterface $userMapper */
            $this->userMapper = $mappers->get('User');
        }
        return $this->userMapper;
    }

    public function logout()
    {
        $this->clearIdentity();
    }
}