<?php
namespace TiaraAdmin\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class UserServiceFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var \TiaraBase\Mapper\MapperManager $mappers */
        $mappers = $serviceLocator->get('MapperManager');
        /** @var \TiaraAdmin\Mapper\UserMapperInterface $users */
        $users = $mappers->get('User');
        /** @var \Zend\Form\FormElementManager $forms */
        $forms = $serviceLocator->get('FormElementManager');

        return new UserService($users, $forms);
    }
}