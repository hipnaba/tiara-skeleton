<?php
namespace TiaraAdmin\Service;

use TiaraAdmin\Entity\UserInterface;
use TiaraAdmin\Mapper\UserMapperInterface;
use Zend\Crypt\Password\Bcrypt;
use Zend\Form\FormElementManager;

/**
 * User management
 *
 * @package TiaraAdmin\Service
 */
class UserService
{
    /** @var UserMapperInterface */
    protected $users;
    /** @var FormElementManager */
    protected $forms;
    /** @var \Zend\Form\Form */
    protected $addUserForm;

    /**
     * Constructor
     *
     * @param UserMapperInterface $userMapper
     * @param FormElementManager $formManager
     */
    function __construct(UserMapperInterface $userMapper, FormElementManager $formManager)
    {
        $this->users = $userMapper;
        $this->forms = $formManager;
    }

    /**
     * Returns all users
     *
     * @return \TiaraBase\Entity\EntityCollectionInterface
     */
    public function getAllUsers()
    {
        return $this->users->fetchAll();
    }

    /**
     * Returns the user with the specified ID
     *
     * @param int $id
     * @return null|UserInterface
     */
    public function getUser($id)
    {
        return $this->users->findById($id);
    }

    /**
     * Persists the user to the database
     *
     * @param array $data
     * @return null|UserInterface
     */
    public function saveUser(array $data)
    {
        $form = $this->getAddUserForm();

        /** @var UserInterface $user */
        $user = $this->users->getEntityPrototype();
        $form->bind($user);

        $form->setData($data);
        if (!$form->isValid()) {
            return null;
        }

        $bcrypt = new Bcrypt();
        $password = $bcrypt->create($user->getPassword());
        $user->setPassword($password);

        $this->users->save($user);
        return $user;
    }

    /**
     * Deletes the user
     *
     * @param UserInterface $user
     */
    public function deleteUser(UserInterface $user)
    {
        $this->users->delete($user);
    }

    /**
     * Returns the add user form
     *
     * @return \TiaraAdmin\Form\AbstractUserForm
     */
    public function getAddUserForm()
    {
        if (null === $this->addUserForm) {
            $this->addUserForm = $this->forms->get('Admin\Form\AddUser');
            $this->addUserForm->setHydrator($this->users->getHydrator());
        }
        return $this->addUserForm;
    }
}