<?php
namespace TiaraAdmin\Entity;

use TiaraBase\Entity\AbstractEntity;

/**
 * Represents a user within the system
 *
 * @package TiaraAdmin\Entity
 */
class User extends AbstractEntity implements
    UserInterface
{
    /** @var string */
    protected $username;
    /** @var string */
    protected $password;

    /**
     * Returns the username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Sets the username
     *
     * @param string $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * Returns the password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Sets the password
     *
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }
}