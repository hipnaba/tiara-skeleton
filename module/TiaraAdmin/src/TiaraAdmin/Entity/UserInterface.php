<?php
namespace TiaraAdmin\Entity;

use TiaraBase\Entity\EntityInterface;

/**
 * Interface UserInterface
 *
 * @package TiaraAdmin\Entity
 */
interface UserInterface extends EntityInterface
{
    /**
     * Returns the username
     *
     * @return string
     */
    public function getUsername();

    /**
     * Sets the username
     *
     * @param string $username
     * @return $this
     */
    public function setUsername($username);

    /**
     * Returns the password
     *
     * @return string
     */
    public function getPassword();

    /**
     * Sets the password
     *
     * @param string $password
     * @return $this
     */
    public function setPassword($password);
}