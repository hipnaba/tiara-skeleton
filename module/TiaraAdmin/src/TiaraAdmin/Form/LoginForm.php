<?php
namespace TiaraAdmin\Form;

use TiaraBase\Form\AbstractForm;

class LoginForm extends AbstractForm
{
    protected $filter = 'Admin\Filter\Login';

    public function init()
    {
        $this->add(array(
            'name' => 'username',
            'type' => 'text',
            'attributes' => array(
                'placeholder' => 'Username'
            )
        ));

        $this->add(array(
            'name' => 'password',
            'type' => 'password',
            'attributes' => array(
                'placeholder' => 'Password'
            )
        ));

        $this->add(array(
            'name' => 'redirect',
            'type' => 'hidden'
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'submit',
            'options' => array(
                'label' => 'Login'
            )
        ));
    }
}