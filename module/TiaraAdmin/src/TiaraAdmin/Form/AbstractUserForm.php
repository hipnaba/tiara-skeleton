<?php
namespace TiaraAdmin\Form;

use TiaraBase\Form\AbstractForm;

class AbstractUserForm extends AbstractForm
{
    /** @var string */
    protected $filter = 'Admin\Filter\User';

    public function init()
    {
        $this->add(array(
            'type' => 'hidden',
            'name' => 'id'
        ));

        $this->add(array(
            'type' => 'text',
            'name' => 'username',
            'options' => array(
                'label' => 'Username'
            ),
            'attributes' => array(
                'placeholder' => 'Enter username',
                'autocomplete' => 'off'
            )
        ));

        $this->add(array(
            'type' => 'password',
            'name' => 'password',
            'options' => array(
                'label' => 'Password'
            ),
            'attributes' => array(
                'placeholder' => 'Enter password',
                'autocomplete' => 'off'
            )
        ));

        $this->add(array(
            'type' => 'password',
            'name' => 'passwordVerify',
            'options' => array(
                'label' => 'Verify password'
            ),
            'attributes' => array(
                'placeholder' => 'Repeat password'
            )
        ));

        $this->add(array(
            'type' => 'submit',
            'name' => 'save',
            'options' => array(
                'label' => 'Save'
            )
        ), array('priority' => -999));

        $this->add(array(
            'type' => 'submit',
            'name' => 'cancel',
            'options' => array(
                'label' => 'Cancel'
            )
        ), array('priority' => -1000));
    }
}