<?php
namespace TiaraAdmin\Form;

class AddUserForm extends AbstractUserForm
{
    public function init()
    {
        parent::init();

        $this->remove('id');

        $this->get('save')->setLabel('Add user');
    }
}