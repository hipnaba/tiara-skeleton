<?php
namespace TiaraAdmin\Validator;

use TiaraAdmin\Mapper\UserMapperInterface;
use Zend\Validator\AbstractValidator;
use Zend\Validator\Exception;

class UniqueUsername extends AbstractValidator
{
    /** @var UserMapperInterface */
    protected $users;

    const USERNAME_EXISTS = 'usernameExists';

    protected $messageTemplates = array(
        self::USERNAME_EXISTS => 'A user with this username already exists'
    );

    /**
     * Constructor
     *
     * @param UserMapperInterface $userMapper
     * @param null $options
     */
    function __construct(UserMapperInterface $userMapper, $options = null)
    {
        parent::__construct($options);

        $this->users = $userMapper;
    }

    /**
     * Returns true if the username doesn't exists in the database or the username is of the
     * user we're trying to save
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param  string $value
     * @param array $context
     * @return bool
     */
    public function isValid($value, $context = array())
    {
        $user = $this->users->findByUsername($value);

        if (null !== $user) {
            if ((isset($context['id']) && $context['id'] != $user->getId()) || !isset($context['id'])) {
                $this->error(static::USERNAME_EXISTS);
                return false;
            }
        }
        return true;
    }
}