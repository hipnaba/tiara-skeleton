<?php
namespace TiaraAdmin\Validator;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class UniqueUsernameFactory implements FactoryInterface
{
    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var \Zend\Validator\ValidatorPluginManager $validators */
        $validators = $serviceLocator;
        $services = $validators->getServiceLocator();
        /** @var \TiaraBase\Mapper\MapperManager $mappers */
        $mappers = $services->get('MapperManager');
        /** @var \TiaraAdmin\Mapper\UserMapperInterface $userMapper */
        $userMapper = $mappers->get('User');

        return new UniqueUsername($userMapper);
    }
}