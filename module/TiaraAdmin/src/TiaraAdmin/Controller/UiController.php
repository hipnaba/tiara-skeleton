<?php
namespace TiaraAdmin\Controller;

use Zend\Mvc\Controller\AbstractActionController;

/**
 * Class UiController
 * @package TiaraAdmin\Controller
 */
class UiController extends AbstractActionController
{
    public function indexAction()
    { }

    public function alertsAction()
    {
        $this->flashMessenger()->addErrorMessage('<h1>Danger</h1>Something <strong>dangerous</strong> happened!');
        $this->flashMessenger()->addWarningMessage('<h2>Warning</h2>Something dangerous <strong>might</strong> happen!');
        $this->flashMessenger()->addInfoMessage('<h3>Information</h3>Something you should <strong>know</strong> about!');
        $this->flashMessenger()->addSuccessMessage('<h4>Success</h4>Something was <strong>successful</strong>!');
    }

    public function panelsAction()
    { }
}