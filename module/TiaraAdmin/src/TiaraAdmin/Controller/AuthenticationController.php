<?php
namespace TiaraAdmin\Controller;

use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractActionController;

/**
 * Class AuthenticateController
 * @package TiaraAdmin\Controller
 * @method Plugin\Authentication authentication()
 */
class AuthenticationController extends AbstractActionController
{
    const DEFAULT_REDIRECT = 'site';

    /** @var \Zend\Form\Form */
    protected $loginForm;

    public function loginAction()
    {
        $form = $this->getLoginForm();

        $flash = $this->flashMessenger()->setNamespace('redirect');
        if ($flash->hasMessages()) {
            $redirect = implode('', $flash->getMessages());
            $form->get('redirect')->setValue($redirect);
        }

        $redirect = $this->url()->fromRoute();
        $prg = $this->prg($redirect, true);

        if ($prg instanceof Response) {
            return $prg;
        }

        if (is_array($prg)) {
            $form->setData($prg);

            if ($form->isValid()) {
                $data = $form->getData();
                if ($this->authentication()->login($data)) {
                    $redirect = isset($data['redirect']) && !empty($data['redirect']) ? $data['redirect'] : $this->url()->fromRoute(self::DEFAULT_REDIRECT);
                    $this->redirect()->toUrl($redirect);
                } else {
                    $this->flashMessenger()->addErrorMessage('<h1 class="icon-forbidden">Bad Credentials</h1>Your username or password are <strong>incorrect</strong>!');
                }
            }
        }

        $this->layout('layout/login');
        return array(
            'form' => $form
        );
    }

    public function logoutAction()
    {
        $this->authentication()->logout();
        $this->redirect()->toUrl($this->url()->fromRoute(self::DEFAULT_REDIRECT));
    }

    /**
     * @return \Zend\Form\Form
     */
    protected function getLoginForm()
    {
        if (null === $this->loginForm) {
            /** @var \Zend\Form\FormElementManager $forms */
            $forms = $this->getServiceLocator()->get('FormElementManager');

            $this->loginForm = $forms->get('Admin\Form\Login');
        }
        return $this->loginForm;
    }
}