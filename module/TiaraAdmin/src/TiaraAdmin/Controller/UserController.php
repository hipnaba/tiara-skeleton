<?php
namespace TiaraAdmin\Controller;

use TiaraAdmin\Service\UserService;
use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractActionController;

/**
 * Class UserController
 *
 * @package TiaraAdmin\Controller
 *
 * @method UserService users()
 */
class UserController extends AbstractActionController
{
    public function indexAction()
    {
        return array(
            'users' => $this->users()->getAllUsers()
        );
    }

    public function addAction()
    {
        $form = $this->users()->getAddUserForm();

        $redirectUrl = $this->url()->fromRoute('admin/users', array('action' => 'add'));
        $prg = $this->prg($redirectUrl, true);

        if ($prg instanceof Response) {
            return $prg;
        }

        if (false !== $prg) {
            if (isset($prg['cancel'])) {
                return $this->redirect()->toUrl($this->url()->fromRoute('admin/users'));
            }

            $user = $this->users()->saveUser($prg);
            if (null !== $user) {
                $this->flashMessenger('tiara-admin')->addSuccessMessage("User added");
                return $this->redirect()->toUrl($this->url()->fromRoute('admin/users'));
            }
        }

        return array(
            'form' => $form
        );
    }

    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', null);
        $user = $this->users()->getUser($id);

        if (null === $user) {
            $this->flashMessenger('tiara-admin')->addWarningMessage('User doesn\'t exist.');
            return $this->redirect()->toUrl($this->url()->fromRoute('admin/users'));
        }

        $redirectUrl = $this->url()->fromRoute('admin/users', array(
            'action' => 'delete',
            'id' => $user->getId()
        ));
        $prg = $this->prg($redirectUrl, true);

        if ($prg instanceof Response) {
            return $prg;
        }

        if ($prg) {
            if ('Yes' == $prg['confirmation']) {
                $this->users()->deleteUser($user);
                $this->flashMessenger('tiara-admin')->addSuccessMessage("User deleted");
            }
            return $this->redirect()->toUrl($this->url()->fromRoute('admin/users'));
        }

        return array('user' => $user);
    }
}