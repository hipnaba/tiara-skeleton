<?php
namespace TiaraAdmin\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class Users extends AbstractPlugin implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /** @var \TiaraAdmin\Service\UserService */
    protected $service;

    /**
     * @return \TiaraAdmin\Service\UserService
     */
    public function __invoke()
    {
        if (null === $this->service) {
            /** @var \Zend\Mvc\Controller\PluginManager $plugins */
            $plugins = $this->getServiceLocator();
            /** @var \Zend\ServiceManager\ServiceLocatorInterface $services */
            $services = $plugins->getServiceLocator();
            $this->service = $services->get('Users');
        }
        return $this->service;
    }
}