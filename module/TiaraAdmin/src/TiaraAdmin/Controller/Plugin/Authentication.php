<?php
namespace TiaraAdmin\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class Authentication extends AbstractPlugin implements
    ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /** @var \TiaraAdmin\Service\AuthenticationService */
    protected $authenticate;

    /**
     * @return \TiaraAdmin\Service\AuthenticationService
     */
    public function __invoke()
    {
        return $this;
    }

    public function login($credentials)
    {
        return $this->getAuthenticationService()->login($credentials);
    }

    public function logout()
    {
        $this->getAuthenticationService()->logout();
    }

    /**
     * @return \TiaraAdmin\Service\AuthenticationService
     */
    private function getAuthenticationService()
    {
        if (null === $this->authenticate) {
            /** @var \Zend\Mvc\Controller\PluginManager $plugins */
            $plugins = $this->getServiceLocator();
            $services = $plugins->getServiceLocator();

            $this->authenticate = $services->get('Authentication');
        }
        return $this->authenticate;
    }
}
