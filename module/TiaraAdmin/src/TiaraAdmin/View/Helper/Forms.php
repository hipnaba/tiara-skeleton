<?php
namespace TiaraAdmin\View\Helper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\View\Helper\AbstractHelper;

class Forms extends AbstractHelper implements
    ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /** @var \Zend\Form\FormElementManager */
    protected $forms;

    public function __invoke($form = null)
    {
        if ($form) {
            return $this->getFormElementManager()->get($form);
        }
        else {
            return $this->getFormElementManager();
        }
    }

    public function getFormElementManager()
    {
        if (null === $this->forms) {
            /** @var \Zend\View\HelperPluginManager $helpers */
            $helpers = $this->getServiceLocator();
            $services = $helpers->getServiceLocator();

            $this->forms = $services->get('FormElementManager');
        }
        return $this->forms;
    }
}