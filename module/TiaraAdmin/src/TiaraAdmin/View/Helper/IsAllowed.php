<?php
namespace TiaraAdmin\View\Helper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\View\Helper\AbstractHelper;

class IsAllowed extends AbstractHelper implements
    ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /** @var \TiaraAdmin\Service\AuthorizationService */
    protected $authorize;

    public function __invoke($resource, $privilege = null)
    {
        if (null === $this->authorize) {
            /** @var \Zend\View\HelperPluginManager $helpers */
            $helpers = $this->getServiceLocator();
            $services = $helpers->getServiceLocator();

            $this->authorize = $services->get('Authorization');
        }
        return $this->authorize->isAllowed($resource, $privilege);
    }
}