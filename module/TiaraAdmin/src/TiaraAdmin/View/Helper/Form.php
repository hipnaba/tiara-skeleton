<?php
namespace TiaraAdmin\View\Helper;

use Zend\Form\FormInterface;
use Zend\Form\View\Helper\Form as ZendForm;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class Form extends ZendForm implements
    ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    public function render(FormInterface $form)
    {
        if (!$form->getAttribute('action')) {
            $form->setAttribute('action', $this->getView()->url(null, array(), true));
        }

        if (!$form->getAttribute('method')) {
            $form->setAttribute('method', 'post');
        }

        $this->getView()->formRow()->setPartial('partials/form/row');

        return parent::render($form);
    }
}