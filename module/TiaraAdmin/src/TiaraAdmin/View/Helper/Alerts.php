<?php
namespace TiaraAdmin\View\Helper;

use Zend\View\Helper\FlashMessenger;

class Alerts extends FlashMessenger
{
    const TYPE_ERROR = 'error';
    const TYPE_WARNING = 'warning';
    const TYPE_INFO = 'info';
    const TYPE_SUCCESS = 'success';

    protected $cssClasses = array(
        self::TYPE_ERROR => 'danger',
        self::TYPE_WARNING => 'warning',
        self::TYPE_INFO => 'info',
        self::TYPE_SUCCESS => 'success'
    );

    public function __invoke($namespace = null)
    {
        $flash = $this->getPluginFlashMessenger($namespace);
        $alerts = '';

        foreach ($this->cssClasses as $type => $class) {
            if ($flash->{'has' . ucfirst($type) . 'Messages'}()) {
                $alerts .= '<div class="alert alert-' . $class . '">';

                $messages = array();
                foreach ($flash->{'get' . ucfirst($type) . 'Messages'}() as $message) {
                    $messages[] = $this->getTranslator()->translate($message);
                }
                $messages[0] = "<h1>{$messages[0]}</h1>";
                $alerts .= implode('<hr>', $messages);
                $alerts .= '</div>';
            }
        }
        return $alerts;
    }
}