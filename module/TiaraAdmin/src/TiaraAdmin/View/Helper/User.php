<?php
namespace TiaraAdmin\View\Helper;

use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\View\Helper\AbstractHelper;

class User extends AbstractHelper implements
    ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    /** @var \TiaraAdmin\Service\AuthenticationService */
    protected $authentication;
    /** @var null|\TiaraAdmin\Entity\UserInterface */
    protected $user;

    /**
     * @return null|\TiaraAdmin\Entity\UserInterface
     */
    public function __invoke()
    {
        return $this->getUser();
    }

    /**
     * @return null|\TiaraAdmin\Entity\UserInterface
     */
    public function getUser()
    {
        if (null === $this->user) {
            $this->user = $this->getAuthentication()->getUser();
        }
        return $this->user;
    }

    /**
     * @return \TiaraAdmin\Service\AuthenticationService
     */
    public function getAuthentication()
    {
        if (null === $this->authentication) {
            /** @var \Zend\View\HelperPluginManager $helpers */
            $helpers = $this->getServiceLocator();
            $services = $helpers->getServiceLocator();

            $this->authentication = $services->get('Authentication');
        }
        return $this->authentication;
    }
}