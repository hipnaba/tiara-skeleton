<?php
namespace TiaraAdmin\Authentication\Adapter;

use TiaraAdmin\Mapper\UserMapperInterface;
use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result;
use Zend\Crypt\Password\Bcrypt;

class TiaraUser implements
    AdapterInterface
{
    protected $userMapper;
    protected $username;
    protected $password;

    public function __construct(UserMapperInterface $userMapper)
    {
        $this->userMapper = $userMapper;
    }

    /**
     * @param mixed $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @param mixed $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * Performs an authentication attempt
     *
     * @return \Zend\Authentication\Result
     * @throws \Zend\Authentication\Adapter\Exception\ExceptionInterface If authentication cannot be performed
     */
    public function authenticate()
    {
        $user = $this->userMapper->findByUsername($this->username);
        if (null === $user) {
            return new Result(Result::FAILURE_IDENTITY_NOT_FOUND, null);
        }

        $bcrypt = new Bcrypt();
        if (!$bcrypt->verify($this->password, $user->getPassword())) {
            return new Result(Result::FAILURE_CREDENTIAL_INVALID, null);
        }

        return new Result(Result::SUCCESS, $user->getId());
    }
}