<?php
namespace TiaraAdmin\Filter;

use Zend\InputFilter\InputFilter;
use Zend\Validator\NotEmpty;

class LoginFilter extends InputFilter
{
    public function init()
    {
        $this->add(array(
            'name' => 'username',
            'filters' => array(
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            NotEmpty::IS_EMPTY => 'Enter your username'
                        )
                    )
                )
            )
        ));

        $this->add(array(
            'name' => 'password',
            'filters' => array(
                array('name' => 'StringTrim'),
            ),
            'validators' => array(
                array(
                    'name' => 'NotEmpty',
                    'options' => array(
                        'messages' => array(
                            NotEmpty::IS_EMPTY => 'Enter your password'
                        )
                    )
                )
            )
        ));
    }
}