��          �   %   �      0  (   1     Z  	   c  .   m     �     �     �     �     �     �     �  '   �               /  
   4     ?     L     `  '   i     �     �     �  �  �  0   �     �     �  4   �     $     -     5     G     W     o     r  *   z     �     �     �     �     �     �     
  2     	   M     W     h                                                                          	                                 
                    A user with this username already exists Add user All users Are you sure you want to delete the user '%s'? Cancel Delete Delete user Enter password Enter username No Password Password is required and can't be empty Passwords do not match Repeat password Save User added User deleted User doesn't exist. Username Username is required and can't be empty Users Verify password Yes Project-Id-Version: tiara-user
POT-Creation-Date: 2014-09-09 08:58+0100
PO-Revision-Date: 2014-09-09 08:59+0100
Last-Translator: Danijel Fabijan <hipnaba@gmail.com>
Language-Team: hipnaba <hipnaba@gmail.com>
Language: hr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop
X-Poedit-Basepath: .
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: /home/hipnaba/projects/tiara-user/language
 Korisnik s ovim korisničkim imenom već postoji Dodaj korisnika Svi korisnici Jeste li sigurni da želite obrisati korisnika '%s'? Odustani Obriši Obriši korisnika Unesite lozinku Unesite korisničko ime Ne Lozinka Lozinka je obavezna i ne može biti prazna Lozinke se ne podudaraju Ponovite lozinku Spremi Korisnik dodan Korisnik obrisan Korisnik ne postoji Korisničko ime Korisničko ime je obavezno i ne može biti prazno Korisnici Provjera lozinke Da 