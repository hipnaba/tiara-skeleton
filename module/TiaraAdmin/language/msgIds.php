<?php
/** Form */
_('Username');
_('Enter username');
_('Password');
_('Enter password');
_('Verify password');
_('Repeat password');
_('Save');
_('Cancel');
_('Add user');

/** Navigation */
_('Users');
_('Add user');
_('Delete user');

/** Validators */
_('A user with this username already exists');
_('Username is required and can\'t be empty');
_('Password is required and can\'t be empty');
_('Passwords do not match');

/** Views */
_('All users');
_('User added');
_('Username');
_('Delete');
_('User doesn\'t exist.');
_('Are you sure you want to delete the user \'%s\'?');
_('Yes');
_('No');
_('User deleted');