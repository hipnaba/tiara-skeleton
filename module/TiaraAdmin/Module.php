<?php
namespace TiaraAdmin;

use TiaraBase\Entity\EntityProviderInterface;
use TiaraBase\Mapper\MapperProviderInterface;
use Zend\EventManager\EventInterface;
use Zend\Http\Response as HttpResponse;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ControllerPluginProviderInterface;
use Zend\ModuleManager\Feature\ControllerProviderInterface;
use Zend\ModuleManager\Feature\FormElementProviderInterface;
use Zend\ModuleManager\Feature\InputFilterProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\ValidatorProviderInterface;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use Zend\Mvc\MvcEvent;

use Zend\View\Model\ViewModel;

class Module implements
    AutoloaderProviderInterface,
    BootstrapListenerInterface,
    ConfigProviderInterface,
    ControllerProviderInterface,
    ControllerPluginProviderInterface,
    EntityProviderInterface,
    FormElementProviderInterface,
    InputFilterProviderInterface,
    MapperProviderInterface,
    ServiceProviderInterface,
    ValidatorProviderInterface,
    ViewHelperProviderInterface
{
    /**
     * Listen to the bootstrap event
     *
     * @param EventInterface $e
     * @return array
     */
    public function onBootstrap(EventInterface $e)
    {
        /** @var \Zend\Mvc\Application $application */
        $application = $e->getTarget();
        $services = $application->getServiceManager();
        $events = $application->getEventManager();

        /** @var \TiaraAdmin\Service\AuthorizationService $authorization */
        $authorization = $services->get('Authorization');
        $authorization->attach($events);

        $events->attach(MvcEvent::EVENT_ROUTE, array($this, 'onRoute'));
        $events->attach(MvcEvent::EVENT_RENDER, array($this, 'onRender'));
    }

    public function onRoute(MvcEvent $event)
    {
        $route = $event->getRouteMatch()->getMatchedRouteName();

        if (preg_match('/^admin/', $route)) {
            $event->getViewModel()->setTemplate('layout/admin');
        }
    }

    public function onRender(MvcEvent $event)
    {
        $result = $event->getResult();
        if ($result instanceof ViewModel) {
            $model = new ViewModel($result->getVariables());
            $model->setTemplate($result->getTemplate());

            $result->addChild($model)
                   ->setTemplate('partials/viewport');
        }
    }

    /**
     * Return an array for passing to Zend\Loader\AutoloaderFactory.
     *
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__
                )
            )
        );
    }

    /**
     * Returns configuration to merge with application configuration
     *
     * @return array|\Traversable
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to seed
     * such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getControllerConfig()
    {
        return array(
            'invokables' => array(
                'Admin\Controller\Authentication' => 'TiaraAdmin\Controller\AuthenticationController',
                'Admin\Controller\Index' => 'TiaraAdmin\Controller\IndexController',
                'Admin\Controller\Ui' => 'TiaraAdmin\Controller\UiController',
                'Admin\Controller\User' => 'TiaraAdmin\Controller\UserController',
            )
        );
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getControllerPluginConfig()
    {
        return array(
            'aliases' => array(
                'Authentication' => 'Admin\Plugin\Authentication',
                'Users' => 'Admin\Plugin\Users',
            ),
            'invokables' => array(
                'Admin\Plugin\Authentication' => 'TiaraAdmin\Controller\Plugin\Authentication',
                'Admin\Plugin\Users' => 'TiaraAdmin\Controller\Plugin\Users',
            )
        );
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getEntityConfig()
    {
        return array(
            'aliases' => array(
                'Admin\Entity\User' => 'TiaraAdmin\Entity\UserInterface',
                /** short */
                'User' => 'Admin\Entity\User',
            ),
            'invokables' => array(
                'TiaraAdmin\Entity\UserInterface' => 'TiaraAdmin\Entity\User',
            )
        );
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getFormElementConfig()
    {
        return array(
            'invokables' => array(
                'Admin\Form\AddUser' => 'TiaraAdmin\Form\AddUserForm',
                'Admin\Form\Login' => 'TiaraAdmin\Form\LoginForm',
            )
        );
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getInputFilterConfig()
    {
        return array(
            'invokables' => array(
                'Admin\Filter\Login' => 'TiaraAdmin\Filter\LoginFilter',
                'Admin\Filter\User' => 'TiaraAdmin\Filter\UserFilter',
            )
        );
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getMapperConfig()
    {
        return array(
            'aliases' => array(
                'Admin\Mapper\User' => 'TiaraAdmin\Mapper\UserMapperInterface',
                /** short */
                'User' => 'Admin\Mapper\User',
            ),
            'invokables' => array(
                'TiaraAdmin\Mapper\UserMapperInterface' => 'TiaraAdmin\Mapper\UserMapper'
            )
        );
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getServiceConfig()
    {
        return array(
            'aliases' => array(
                'AdminNavigation' => 'Admin\Navigation\Admin',
                'Authentication' => 'Admin\Service\Authentication',
                'Authorization' => 'Admin\Service\Authorization',
                'Users' => 'Admin\Service\Users',
            ),
            'factories' => array(
                'Admin\Navigation\Admin' => 'TiaraAdmin\Service\AdminNavigationFactory',
                'Admin\Service\Users' => 'TiaraAdmin\Service\UserServiceFactory',
            ),
            'invokables' => array(
                'Admin\Service\Authentication' => 'TiaraAdmin\Service\AuthenticationService',
                'Admin\Service\Authorization' => 'TiaraAdmin\Service\AuthorizationService',
            )
        );
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getValidatorConfig()
    {
        return array(
            'aliases' => array(
                'UniqueUsername' => 'Admin\Validator\UniqueUsername',
            ),
            'factories' => array(
                'Admin\Validator\UniqueUsername' => 'TiaraAdmin\Validator\UniqueUsernameFactory',
            )
        );
    }

    /**
     * Expected to return \Zend\ServiceManager\Config object or array to
     * seed such an object.
     *
     * @return array|\Zend\ServiceManager\Config
     */
    public function getViewHelperConfig()
    {
        return array(
            'aliases' => array(
                'Alerts' => 'Admin\Helper\Alerts',
                'Exception' => 'Admin\Helper\Exception',
                'Form' => 'Admin\Helper\Form',
                'Forms' => 'Admin\Helper\Forms',
                'IsAllowed' => 'Admin\Helper\IsAllowed',
                'User' => 'Admin\Helper\User',
            ),
            'invokables' => array(
                'Admin\Helper\Alerts' => 'TiaraAdmin\View\Helper\Alerts',
                'Admin\Helper\Exception' => 'TiaraAdmin\View\Helper\Exception',
                'Admin\Helper\Form' => 'TiaraAdmin\View\Helper\Form',
                'Admin\Helper\Forms' => 'TiaraAdmin\View\Helper\Forms',
                'Admin\Helper\IsAllowed' => 'TiaraAdmin\View\Helper\IsAllowed',
                'Admin\Helper\User' => 'TiaraAdmin\View\Helper\User',
            )
        );
    }
}