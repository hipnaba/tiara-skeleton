<?php
return array(
    'admin' => array(
        'home' => array(
            'label' => 'Home',
            'route' => 'admin',
            'resource' => 'route/admin',
            'pages' => array(
                array(
                    'label' => 'Users',
                    'route' => 'admin/users',
                    'pages' => array(
                        array(
                            'label' => 'Add user',
                            'route' => 'admin/users',
                            'params' => array(
                                'action' => 'add'
                            )
                        ),
                        array(
                            'label' => 'Delete user',
                            'route' => 'admin/users',
                            'visible' => false,
                            'params' => array(
                                'action' => 'delete'
                            )
                        ),
                    )
                )
            )
        )
    )
);