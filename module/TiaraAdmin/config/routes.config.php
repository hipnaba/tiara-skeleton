<?php
return array(
    // The admin route. Top route for all admin routes.
    // Everything else is added to child_routes of this route.
    'admin' => array(
        'type' => 'Literal',
        'options' => array(
            'route' => '/admin',
            'defaults' => array(
                'controller' => 'Admin\Controller\Index',
                'action' => 'index'
            )
        ),
        'may_terminate' => true,
        // All routes within the admin need to be contained here
        'child_routes' => array(
            // demo route
            'ui' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/ui[/:action]',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\Ui',
                        'action' => 'index',
                    ),
                    'constraints' => array(
                        'action' => '[a-z-]+',
                    )
                )
            ),
            'users' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/users[/:action[/:id]]',
                    'defaults' => array(
                        'controller' => 'Admin\Controller\User',
                        'action' => 'index'
                    ),
                    'constraints' => array(
                        'action' => '[a-z-]+',
                        'id' => '[0-9]+',
                    )
                )
            )
        ),
    ),
    // AuthenticateController
    'login' => array(
        'type' => 'literal',
        'options' => array(
            'route' => '/login',
            'defaults' => array(
                'controller' => 'Admin\Controller\Authentication',
                'action' => 'login'
            )
        )
    ),
    'logout' => array(
        'type' => 'literal',
        'options' => array(
            'route' => '/logout',
            'defaults' => array(
                'controller' => 'Admin\Controller\Authentication',
                'action' => 'logout'
            )
        )
    )
);