<?php
return array(
    'navigation' => include __DIR__ . '/navigation.config.php',

    // Router configuration
    'router' => array(
        'routes' => include __DIR__ . '/routes.config.php'
    ),

    'translation_file_patterns' => array(
        array(
            'type'     => 'gettext',
            'base_dir' => __DIR__ . '/../language',
            'pattern'  => '%s.mo',
        ),
    ),

    // View manager configuration
    'view_manager' => array(
        'doctype' => 'HTML5',

        'display_not_found_reason' => (bool) ini_get('display_errors'),
        'not_found_template' => 'error/404',

        'display_exceptions' => (bool) ini_get('display_errors'),
        'exception_template' => 'error/exception',

        'template_map' => array(
            'admin/main-menu' => __DIR__ . '/../view/partials/main-menu.phtml',
            'admin/viewport' => __DIR__ . '/../view/partials/viewport.phtml',
            'error/exception' => __DIR__ . '/../view/error/exception.phtml',
        ),

        'template_path_stack' => array(
            __DIR__ . '/../view'
        )
    )
);