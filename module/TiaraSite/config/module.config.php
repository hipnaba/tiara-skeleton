<?php
return array(
    'controllers' => array(
        'invokables' => array(
            'Site\Controller\Index' => 'TiaraSite\Controller\IndexController'
        )
    ),

    'router' => array(
        'routes' => array(
            'site' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        'controller' => 'Site\Controller\Index',
                        'action' => 'index'
                    )
                )
            )
        )
    ),

    'view_manager' => array(
        'layout' => 'layout/layout',

        'template_path_stack' => array(
            __DIR__ . '/../view'
        )
    )
);