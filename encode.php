<?php
echo base64_encode('<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
    width="32.812px" height="30.1px" viewBox="0 0 32.812 30.1" style="enable-background:new 0 0 32.812 30.1;" xml:space="preserve">
    <path fill="currentColor" style="fill:currentColor;" d="M0,0v16.743h5.249v7.154v3.617V30.1h3.61v-2.586h15.423V30.1h3.606V16.743h4.923V0H0z M31.136,0.925
    l-7.764,14.658h-4.783l7.762-14.658H31.136z M23.016,0.889l-7.766,14.66h-4.785l7.767-14.66H23.016z M1.31,1.073l5.617-0.017
    L1.281,11.634L1.31,1.073z M7.468,15.704H2.684l7.763-14.658h4.785L7.468,15.704z M24.282,23.897H8.859v-7.154h15.423V23.897z
    M32.149,15.455l-5.616-0.006l5.646-10.576L32.149,15.455z"/>
    <path fill="currentColor" style="fill:currentColor;" d="M0,0v16.743h5.249v7.154v3.617V30.1h3.61v-2.586h15.423V30.1h3.606V16.743h4.923V0H0z M31.136,0.925
    l-7.764,14.658h-4.783l7.762-14.658H31.136z M23.016,0.889l-7.766,14.66h-4.785l7.767-14.66H23.016z M1.31,1.073l5.617-0.017
    L1.281,11.634L1.31,1.073z M7.468,15.704H2.684l7.763-14.658h4.785L7.468,15.704z M24.282,23.897H8.859v-7.154h15.423V23.897z
    M32.149,15.455l-5.616-0.006l5.646-10.576L32.149,15.455z"/>
    <g>
    </g>
    <g>
    </g>
    <g>
    </g>
    <g>
    </g>
    <g>
    </g>
    <g>
    </g>
    <g>
    </g>
    <g>
    </g>
    <g>
    </g>
    <g>
    </g>
    <g>
    </g>
    <g>
    </g>
    <g>
    </g>
    <g>
    </g>
    <g>
    </g>
    </svg>');