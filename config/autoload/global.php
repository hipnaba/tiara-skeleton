<?php
return array(
    // Zend\Db\Adapter\Adapter configuration
    'db' => array(
        // The driver used to connect to the database
        'driver' => 'Pdo',
        // Driver options
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
        )
    ),

    // Default services
    'service_manager' => array(
        'factories' => array(
            'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory'
        )
    ),
);